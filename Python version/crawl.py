import json
import requests
import time

def getUserStatus(handle):
    response = requests.get("https://codeforces.com/api/user.status?handle=" + handle)
    userStatus = response.json()

    try:
        if userStatus["status"] != "OK":
            print("Status not OK!")
    except KeyError:
        print("Status not found!")

    return userStatus["result"]

def countProblemSolvedByMonths(userStatus):
    count = dict()
    for status in userStatus:
        try:
            verdict = status["verdict"]
        except KeyError:
            print("Verdict not found!")

            if verdict != "OK":
            continue

        try:
            creationTimeSeconds = status["creationTimeSeconds"]
        except KeyError:
            print("CreationTimeSeconds not found!")
        
        year = time.localtime(creationTimeSeconds).tm_year 
        month = time.localtime(creationTimeSeconds).tm_mon
        try:
            count[(year,month)] += 1
        except KeyError:
            count[(year,month)] = 1
    return count

def sortCountByDate(count):
    myList = list()
    for it in count:
        myList.append((count[it], it))
    myList.sort(key = lambda x: x[1], reverse = True)
    return myList

userStatus = getUserStatus("denis2111")
count = countProblemSolvedByMonths(userStatus)
problemsSolvedByMonths = sortCountByDate(count)


print(count)
