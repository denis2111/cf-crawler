from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from pprint import pprint

import json
import requests
import time


SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
SPREADSHEET_ID = '1WAqLU1oBH58O_cbkzrJWFXIRSUlIKKo3bVZBv1s1mVA'
RANGE_NAME = 'denis2111'

def getUserStatus(handle):
    response = requests.get("https://codeforces.com/api/user.status?handle=" + handle)
    userStatus = response.json()

    try:
        if userStatus["status"] != "OK":
            print("Status not OK!")
    except KeyError:
        print("Status not found!")

    return userStatus["result"]

def countProblemSolvedByMonths(userStatus):
    count = dict()
    for status in userStatus:
        try:
            verdict = status["verdict"]
        except KeyError:
            print("Verdict not found!")

        if verdict != "OK":
            continue

        try:
            creationTimeSeconds = status["creationTimeSeconds"]
        except KeyError:
            print("CreationTimeSeconds not found!")
        
        year = time.localtime(creationTimeSeconds).tm_year 
        month = time.localtime(creationTimeSeconds).tm_mon
        try:
            count[(year,month)] += 1
        except KeyError:
            count[(year,month)] = 1
    return count

def sortCountByDate(count):
    myList = list()
    for it in count:
        myList.append((count[it], it))
    myList.sort(key = lambda x: x[1], reverse = True)
    return myList

def getSpredsheetCreds():
    creds = None
    
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    return creds

def writeProblemsSolvedByMonthsToSpreadsheet(creds, problemsSolvedByMonths):
    service = build('sheets', 'v4', credentials=creds)
    # myValueInputOption = valueInputOption

    values = [
        ["Date", "Number"]
    ]
    for month in problemsSolvedByMonths:
        values.append([str(month[1][0]) + '/' + str(month[1][1]), month[0]])

    myValueRangeBody = {
        'values': values
    }
    request = service.spreadsheets().values().update(spreadsheetId=SPREADSHEET_ID, range = RANGE_NAME + "!A1:B1000",
                                        valueInputOption = 'RAW', body = myValueRangeBody)
    response = request.execute()
    pprint(response)

userStatus = getUserStatus("denis2111")
count = countProblemSolvedByMonths(userStatus)
problemsSolvedByMonths = sortCountByDate(count)

creds = getSpredsheetCreds()
writeProblemsSolvedByMonthsToSpreadsheet(creds, problemsSolvedByMonths)

# print(count)
