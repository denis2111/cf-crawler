#just an example of how looks an user status object
[{
    "id":56927462,
    "contestId":1190,
    "creationTimeSeconds":1562947726,
    "relativeTimeSeconds":5626,
    "problem":{
        "contestId":1190,
        "index":"C",
        "name":"Tokitsukaze and Duel",
        "type":"PROGRAMMING",
        "points":1500.0,
        "tags":["binary search","brute force","data structures","greedy"]
    },
    "author":{
        "contestId":1190,
        "members":[{"handle":"denis2111"}],
        "participantType":"CONTESTANT",
        "ghost":false,
        "room":24,
        "startTimeSeconds":1562942100
    },
    "programmingLanguage":"GNU C++17",
    "verdict":"OK",
    "testset":"TESTS",
    "passedTestCount":60,
    "timeConsumedMillis":46,
    "memoryConsumedBytes":1126400
}