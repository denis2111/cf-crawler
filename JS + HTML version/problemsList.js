function getUserStatus(handle){
  console.log("getUserStatus");
  var xhttp = new XMLHttpRequest;
  xhttp.onreadystatechange = function() {
    if(xhttp.readyState === 4){
      var userStatus = JSON.parse(xhttp.responseText);

      if (userStatus["status"] != "OK")
        window.alert("Request status is not OK!");
      userStatus = userStatus["result"];
      
      countProblemSolved(userStatus);
    }
  };
  xhttp.open("GET", "https://codeforces.com/api/user.status?handle=" + handle, true);
  xhttp.send();
}

var problemSolved = {};

function getAllProblems() {
  url_call = "http://codeforces.com/api/problemset.problems"
  var xhttp = new XMLHttpRequest;

  xhttp.onreadystatechange = function() {
    if(xhttp.readyState === 4){
      console.log(problemSolved);
      
      var body = document.getElementById("content");
      body.innerHTML = "";
      for(i = 0; i < 4000; i += 100) {
        var tbl = document.createElement("table");
        tbl.id=i;
        tbl.style.fontSize = "small";
        body.appendChild(document.createTextNode(i));
        body.appendChild(tbl);
      }
      
      try{
        var problems = JSON.parse(xhttp.responseText)["result"]["problems"];
      }
      catch{
        console.log("Can't parse JSON!");
      }

      for (var i = problems.length - 1; i >= 0; i--) {
        var difficulty
        try{
          difficulty = problems[i]["rating"];
        }
        catch{
          difficulty = 0;
        }
        if(difficulty === undefined)
          difficulty = 0
        
        var table = document.getElementById(difficulty);
        var row = table.rows[table.rows.length - 1];

        if(table.rows.length === 0 || row.cells.length >= 15)
          row = table.insertRow(-1);
        cell = row.insertCell()
      
        var c = problems[i].contestId
        var ind = problems[i].index 
        var name = c + ind
        var h = "<a href = \"https://codeforces.com/contest/" + c + "/problem/" + ind + "\"> " + name + "<\a>";
        if(name in problemSolved) {
          cell.style.backgroundColor = "#7ae872";
        }
        cell.innerHTML = h
      }
    }
  };
  
  xhttp.open("GET", url_call, true);
  xhttp.send();
}

function countProblemSolved(userStatus){
  for (status in userStatus){
    try{
      var verdict = userStatus[status]["verdict"];
    }
    catch{
      continue;
    }

    if (verdict != "OK")
      continue;

    try{
      var problemID = userStatus[status]["problem"]["contestId"].toString() + userStatus[status]["problem"]["index"];
      problemSolved[problemID] = 1;
    }
    catch{
      continue;
    }
  }
  getAllProblems();
}

function main(){
  console.log("main");
  problemSolved = {};
  getUserStatus(document.getElementById("handleInput").value, getAllProblems);
}