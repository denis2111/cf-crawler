const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

function printTable(countProblemSolvedByMonthsDic){
  var div = document.getElementById("content");
  div.innerHTML = "";
  var table = document.createElement("table");
  div.appendChild(table);
  var tableHead = table.insertRow(0);
  tableHead.insertCell(0).innerHTML = "Year";
  tableHead.insertCell(1).innerHTML = "Month";
  tableHead.insertCell(2).innerHTML = "Count";
  tableHead.insertCell(3).innerHTML = "&lt;1500"; 
  tableHead.cells[tableHead.cells.length - 1].id = "lessThen1500";
  tableHead.insertCell(4).innerHTML = "&lt;2000"; 
  tableHead.cells[tableHead.cells.length - 1].id = "lessThen2000"; 
  tableHead.insertCell(5).innerHTML = "&lt;2500"; 
  tableHead.cells[tableHead.cells.length - 1].id = "lessThen2500"; 
  tableHead.insertCell(6).innerHTML = "2500+"; 
  tableHead.cells[tableHead.cells.length - 1].id = "greaterThen2500"; 
  
  while (table.rows.length > 1)
    table.deleteRow(table.rows.length - 1);

  var totalProblems = 0;
  var totalLessThen1500 = 0;
  var totalLessThen2000 = 0;
  var totalLessThen2500 = 0;
  var totalGreaterThen2500 = 0;
  for (element in countProblemSolvedByMonthsDic){
    var row = table.insertRow(1);

    var year = row.insertCell(0);
    var month = row.insertCell(1);
    var number = row.insertCell(2);
    var lessThen1500 = row.insertCell(3);
    var lessThen2000 = row.insertCell(4);
    var lessThen2500 = row.insertCell(5);
    var greaterThen2500 = row.insertCell(6);
    
    year.innerHTML = Math.floor(element / 100);
    month.innerHTML = monthNames[element % 100];
    number.innerHTML = countProblemSolvedByMonthsDic[element]["number"];
    lessThen1500.innerHTML = countProblemSolvedByMonthsDic[element]["<1500"];
    lessThen2000.innerHTML = countProblemSolvedByMonthsDic[element]["<2000"];
    lessThen2500.innerHTML = countProblemSolvedByMonthsDic[element]["<2500"];
    greaterThen2500.innerHTML = countProblemSolvedByMonthsDic[element]["2500+"];
    totalProblems += countProblemSolvedByMonthsDic[element]["number"];
    totalLessThen1500 += countProblemSolvedByMonthsDic[element]["<1500"];
    totalLessThen2000 += countProblemSolvedByMonthsDic[element]["<2000"];
    totalLessThen2500 += countProblemSolvedByMonthsDic[element]["<2500"];
    totalGreaterThen2500 += countProblemSolvedByMonthsDic[element]["2500+"];
  }

  var row = table.insertRow(table.rows.length);

  var year = row.insertCell(0);
  var month = row.insertCell(1);
  var number = row.insertCell(2);
  var lessThen1500 = row.insertCell(3);
  var lessThen2000 = row.insertCell(4);
  var lessThen2500 = row.insertCell(5);
  var greaterThen2500 = row.insertCell(6);

  year.innerHTML = "Total";
  number.innerHTML = totalProblems;
  lessThen1500.innerHTML = totalLessThen1500;
  lessThen2000.innerHTML = totalLessThen2000;
  lessThen2500.innerHTML = totalLessThen2500;
  greaterThen2500.innerHTML = totalGreaterThen2500;
}

var userStatusUpdated = 0;
var userStatus = {};
function getUserStatus(handle){
  userStatusUpdated = 0;
  console.log("getUserStatus");
  var xhttp = new XMLHttpRequest;
  xhttp.onreadystatechange = function() {
    if(xhttp.readyState === 4){
      userStatus = JSON.parse(xhttp.responseText);

      if (userStatus["status"] != "OK")
        window.alert("Request status is not OK!");
      userStatus = userStatus["result"];
      userStatusUpdated = 1;
      countProblemSolvedByMonths(userStatus);
    }
  };
  xhttp.open("GET", "https://codeforces.com/api/user.status?handle=" + handle, true);
  xhttp.send();
}

var problemCounted = {};
function problemIsCounted(problem){
  try{
    var problemID = problem["contestId"].toString() + problem["index"];
  }
  catch{
    // something is wrong with this problem, so you must to skip it
    return true;
  }
  if (problemID in problemCounted) 
    return true;
  problemCounted[problemID] = 1;
  return false;
}

function isGym(problem){
  if (problem["contestId"] > 100000)
    return true;
  return false;
}

function addNewProblem(counter, date, difficulty){
  if (!(date in counter)){
    counter[date] = {"number": 0, "<1500": 0, "<2000": 0, "<2500": 0, "2500+": 0};
  }
  
  counter[date]["number"]++;
  if (difficulty < 1500)
    counter[date]["<1500"]++;
  else if (difficulty < 2000)
    counter[date]["<2000"]++;
  else if (difficulty < 2500)
    counter[date]["<2500"]++;
  else
    counter[date]["2500+"]++;
}

function countProblemSolvedByMonths(userStatus){
  var countProblemSolvedByMonthsDic = {};
  for (status in userStatus){
    try{
      var verdict = userStatus[status]["verdict"];
    }
    catch{
      continue;
    }

    if (verdict != "OK")
      continue;
  
    // check if this problem was already counted
    if (problemIsCounted(userStatus[status]["problem"]))
      continue;

    if (isGym(userStatus[status]["problem"]))
      continue;
    
    var creationTimeSeconds = userStatus[status]["creationTimeSeconds"];
    var date = new Date();
    date.setTime(creationTimeSeconds * 1000);
    var year = date.getFullYear();
    var month = date.getMonth();
    var difficulty;
    try{
      difficulty = userStatus[status]["problem"]["rating"];
    }
    catch{
      difficulty = 0;
    }

    addNewProblem(countProblemSolvedByMonthsDic, year * 100 + month, difficulty);
  }

  printTable(countProblemSolvedByMonthsDic);
}

function main(){
  console.log("main");
  problemCounted = {};
  getUserStatus(document.getElementById("handleInput").value);
}